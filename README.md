# dot-lua (pronounced `~/.lua`)

* </i>[wiki](https://gitlab.com/mambrus/dot-lua/-/wikis/Home)</i>
* </i>[ldoc](http://www.ambrus.se/~michael/ldoc/dot-lua/)</i>
* </i>[ldoc ctables](http://www.ambrus.se/~michael/ldoc/lua-ctables/)</i>

This mini-project is a collection of Lua scripts and possibly, albeit less
likely, pre-compiled modules.

It's a personalized continuation of the excellent 
[Play With Lua!](http://www.playwithlua.com/?p=64) write-up written by 
<i>**Ross Andrews** (`randrews` May 22nd, 2015 at 12:00 pm)</i>

Love your work Ross!

## Set-up

Clone this repo but into a specific destination

``` bash
cd $HOME
git clone https://gitlab.com/mambrus/dot-lua.git .lua
```

### Make your Lua VM use it

Assuming you have Lua installed (by any means, all goes), Set the key
environment variable `LUA_INIT` to point to a specific file inside the
project:

``` bash
export LUA_INIT="@$HOME/.lua/init.lua"
```
Done! 

<i>Everything else is set-up by that init-script. No need to clobber with
options and tweaking any other Lua path-variable or require option.</i>

## Pre-requisites

### System

`Luarocks` installed by `luarocks`

``` bash
sudo apt install liblua5.3-dev luarocks
sudo luarocks install luarocks
```

From this point onward, `sudo` for `luarocks` is not needed anymore as all
`rocks` go into `$HOME/.luarocks`

**Note:** Do not install Lua with anything else than with your systems
installer and your systems installer to select Lua version.

### Rocks used by `dot-lua`

Not really pre-requisites, but `init.lua` calls `require` on them, i.e. Lua
with refuse to start without them. Install or remark (they are indeed good
to have though):

``` bash
luarocks install inspect
luarocks install lpeg
luarocks install middleclass
luarocks install lua-help
luarocks install lua-ctables
```

### Other good-to-have

``` bash
luarocks install dkjson
luarocks install luaposix
```
### LDoc where available
* [luaposix](https://luaposix.github.io/luaposix/)
* [dot-lua](http://www.ambrus.se/~michael/ldoc/dot-lua/) This project i.e.

## External references
* [Play With Lua](http://www.playwithlua.com/)
* [Luarocks](https://luarocks.org/) - has good searching
* [Cheet-sheet](https://devhints.io/lua)
* [Configire Neo-vim](https://neovim.io/doc/user/lua.html)
* [Lua users wiki](http://lua-users.org/wiki/)
  * [Lua vs Python](http://lua-users.org/wiki/LuaVersusPython) - Good read
* [Lua language write-up](https://stackoverflow.com/a/2102399/2127708) - The best
* [lDoc manual](https://stevedonovan.github.io/ldoc/manual/doc.md.html)
* [lua.org](https://www.lua.org/)  ofc....


