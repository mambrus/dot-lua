--[[--
My Lua VM start-up initialization

## References:

* Source: [init.lua](https://gitlab.com/mambrus/dot-lua/-/blob/master/init.lua)
* Project: [dot-lua](https://gitlab.com/mambrus/dot-lua)
* `dot-lua` [wiki](https://gitlab.com/mambrus/dot-lua/-/wikis/home)
* Inspired by: [Play With Lua](http://www.playwithlua.com/?p=64)
* [`slembcke/debugger.lua`](https://github.com/slembcke/debugger.lua)

## Hints

### debugger

To make [`debugger`](https://github.com/slembcke/debugger.lua) work without
installing from luarocks, clone and add link to somewhere package.path is
set, like to here in `~/.lua` for example.

@script init.lua

@usage
export LUA_INIT="@$HOME/.lua/init.lua"

--]]

package.path = os.getenv("HOME") .. "/.lua/?.lua;" .. package.path
package.cpath = os.getenv("HOME") .. "/.lua/?.so;" .. package.cpath

package.path = os.getenv("HOME") .. "/wip/tplog/luabin/?.lua;" .. package.path
package.cpath = os.getenv("HOME") .. "/wip/tplog/luabin/?.so;" .. package.cpath
require("Logger")

--package.path = os.getenv("HOME") .. "wip/xb-lua/lua-libs/lua-pahole/luabin/?.lua;" .. package.path
--package.cpath = os.getenv("HOME") .. "wip/xb-lua/lua-libs/lua-pahole/luabin/?.so;" .. package.cpath
--require("Pahole")


inspect = require 'inspect'
class = require 'middleclass'
lpeg = require 'lpeg'
--re = require 're'
--dbg = require 'debugger'

--[[
-- A safter alternative method not relying on the search order above
dofile(os.getenv("HOME") .. "/.lua/Vector.lua")
dofile(os.getenv("HOME") .. "/.lua/Help.lua")
--]]
require("Vector")
require("Help")

function using(namespace, env)
    env = env or _ENV
    if not getmetatable(env) then setmetatable(env, {}) end
    local mt = getmetatable(env)

    if not mt.__mixins then
        mt.__mixins = {}

        mt.__index = function(_, key)
            for _, module in ipairs(mt.__mixins) do
                if module[key] then return module[key] end
            end
        end
    end

    table.insert(mt.__mixins, namespace)
end
