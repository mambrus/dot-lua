#!/usr/bin/env lua

--[[--
Test script for class::Vector:vmul multiplication

* Source: [Vector-vmul_test.lua](https://gitlab.com/mambrus/dot-lua/-/blob/master/bin/Vector-vmul_test.lua)

Example from shell:

``` bash
$ export PATH=$HOME/.lua/bin:$PATH
$ Vector-vmul_test.lua 3 5 4 3

>>> Vector multiplication for pre-configured matrices

>>> Test 1
>>> m:  {3, 2}
{{1, 2, 3}, {2, 3, 4}}

>>> n:  {2, 3}
{{2, 3}, {3, 4}, {4, 5}}

>>> m * n:
{{20, 26}, {29, 38}}

>>> n * m:
{{8, 13, 18}, {11, 18, 25}, {14, 23, 32}}


>>> Test 2
>>> A:  {3, 5}
{{1, 2, 3}, {2, 3, 4}, {3, 4, 5}, {4, 5, 6}, {5, 6, 7}}

>>> B:  {4, 3}
{{4, 3, 2, 1}, {5, 4, 3, 2}, {6, 5, 4, 3}}

>>> A * B:
{{32, 26, 20, 14}, {47, 38, 29, 20}, {62, 50, 38, 26}, {77, 62, 47, 32}, {92, 74, 56, 38}}

:

```

@script Vector-vmul_test.lua

@see Vector

@usage  Run matrix multiplications tests
loadfile("Vector-vector_test.lua")(cols1, rows1, cols2, rows2)
--]]

require("Vector")
--dofile("Vector.lua")


local args = {...} -- loadfile arguments. Note that args will never be nil
                   -- as even an empty table is not nil

local v   = Vector(1,2,3)
local b   = Vector(4,3,2,1)
local m   = Vector(v,1+v)
local n   = ~(m+1)
local A   = Vector(v, v+1, v+2, v+3, v+4)
local B   = Vector(b, b+1, b+2)

print(">>> Vector multiplication for pre-configured matrices")
print()
print(">>> Test 1")
print(">>> m:", m:dim())
print(m)
print()
print(">>> n:", n:dim())
print(n)
print()
print(">>> m * n:")
print(m * n)
print()
print(">>> n * m:")
print(n * m)
print()

print()
print(">>> Test 2")
print(">>> A:", A:dim())
print(A)
print()
print(">>> B:", B:dim())
print(B)
print()
print(">>> A * B:")
print(A * B)
print()
print(">>> B * A: Tests rule break, i.e. fail is test OK")
print(B * A)
print()

if #args > 0 then
print()
if #args < 4 or #args > 4 then
    print("Exactly 4 arguments expected for user-defined matrix multiplication test:")
    print("cols1, rows1, cols2, rows2")
    return 1
end
print(">>> Test 3 - user provided dimension sizes")
-- Build-up a 1D vector
local p = Vector()
for c=1, args[1] do
    p:insert(c)
end
-- Build-up Matrix
local P = Vector()
for c=1, args[2] do
    P:insert(c-1+p)
end

-- Build-up a 1D vector
local q = Vector()
for c=1, args[3] do
    q:insert(c)
end
-- Build-up Matrix
local Q = Vector()
for c=1, args[4] do
    Q:insert(c-1+q)
end

print(">>> P:", P:dim())
print(P)
print()
print(">>> Q:", Q:dim())
print(Q)
print()
print(">>> P * Q:")
print(P * Q)
print()
print(">>> Q * P:")
print(Q * P)
print()

end

-- vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4
