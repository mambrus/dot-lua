#!/usr/bin/env lua

--[[--
Test script for class::Vector

Each numeric argument is the size of corresponding dimension

Number of arguments determine the number of dimensions

* Source: [Vector-test.lua](https://gitlab.com/mambrus/dot-lua/-/blob/master/bin/Vector-test.lua)

Example from shell:

``` bash
$ export PATH=$HOME/.lua/bin:$PATH
$ Vector-test.lua 5 3 2
3-D matrix of dimensions        {5, 3, 2}
{{{1, 2, 3, 4, 5}, {1, 2, 3, 4, 5}, {1, 2, 3, 4, 5}}, {{1, 2, 3, 4, 5}, {1, 2, 3, 4, 5}, {1, 2, 3, 4, 5}}}

2-D matrix of dimensions        {5, 3}
{{1, 2, 3, 4, 5}, {1, 2, 3, 4, 5}, {1, 2, 3, 4, 5}}

1-D matrix of dimensions        {5}
{1, 2, 3, 4, 5}

1-D Transpose test: v -> vT -> (vT)T:
v:      {1, 2, 3, 4, 5}
vT:     {{1}, {2}, {3}, {4}, {5}}
(vT)T:  {1, 2, 3, 4, 5}


2-D Transpose test: v -> vT -> (vT)T:
v:      {{1, 2, 3, 4, 5}, {1, 2, 3, 4, 5}, {1, 2, 3, 4, 5}}
vT:     {{1, 1, 1}, {2, 2, 2}, {3, 3, 3}, {4, 4, 4}, {5, 5, 5}}
(vT)T:  {{1, 2, 3, 4, 5}, {1, 2, 3, 4, 5}, {1, 2, 3, 4, 5}}

## Higher dimensions transpose currently failing (not shown)
```

@script Vector-test.lua

@see Vector

@usage  Create a 5x4x3x2 dimensional array and test it
loadfile("Vector-test.lua")(5,4,3,2)
--]]

require("Vector")

local args = {...} -- loadfile arguments. Note that args will never be nil
                   -- as even an empty table is not nil
if #args > 0 then
    local t = Vector(); -- New "Table" of Vectors
    for i, n in pairs(args) do
        t[i] = Vector();
        if i == 1 then
            -- First level contains numbers
            for j=1,n do
                --print("Inserting " .. j .. "(" .. n .. ")")
                t[i]:insert(j);
            end
        else
            -- continuous levels contains the n of the (n-1)D-Vector before
            for j=1,n do
                t[i]:insert(t[i-1]);
            end
        end
    end
    for i, n in pairs(args) do
        print()
        local D = #args - i + 1;
        local v = t[D];
        local dim = v:dim();
        print(D .."-D matrix of dimensions", dim);
        print(v)
    end
    for i, n in pairs(args) do
        if i < 3 then
            print()
            local v = {}
            if i == 1 then
                v = t[1]
            else
                v = t[i]
            end

            local vt = ~v
            print(i .. "-D Transpose test: v -> vT -> (vT)T:")
            print("v:    ", v)
            print("vT:   ", vt)
            print("(vT)T:", ~vt)
        end
    end
else
    print("Provide arguments to run tests")
    print("Each argument is the size of corresponding dimension")
    print()
    print("Example: to test a 6D matrix 6x3x4x2x3x2:")
    print("$ Vector-test.lua 6 3 4 2 3 2")
    print("> loadfile(\"Vector-test.lua\")(6, 3, 4, 2, 3, 2)")
end

-- vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4
