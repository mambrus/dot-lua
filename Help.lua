--[[--
A user friendlier API to the module
[`lua-help`](https://gitlab.com/mambrus/lua-help)

* Source: [Help.lua](https://gitlab.com/mambrus/dot-lua/-/blob/master/Help.lua)

User gives `0..2` arguments to class `Help`'s constructor, which are
interpreted as exact strings or regular expressions depending on which will
give the most specific output.

Different methods are tried where arguments are interpreted either literary
or as a regular expression, one or two (or none) is passed to `Help`'s
constructor until the best possible match is output.

I.e. output is either a text for a specific <b>`subject`</b> or a list of
<b>`topics`</b> if no exact mach is found.

## Prerequisites:

Install the `lua-help` back-end

``` bash
$ luarocks install lua-help
```
Installs the lua-help module for your Lua VM's version {5.1 ... 5.4}
For each build-step dependency failure, follow instructions (circa 4
iterations)

@classmod Help

@usage Help()
-- Returns/prints intro

@usage Help("dofile")
-- Successful exact match in any topic - returning content
   dofile ([filename])

   Opens the named file and executes its contents as a Lua chunk. When
   called without arguments, dofile executes the contents of the standard
   input (stdin). Returns all values returned by the chunk. In case of
   errors, dofile propagates the error to its caller (that is, dofile does
   not run in protected mode)

@usage Help("file")
-- Unsuccessful exact match in any topic - returning suggestions instead
1       caux    luaL_dofile
2       caux    luaL_fileresult
3       caux    luaL_loadfile
4       caux    luaL_loadfilex
5       funcs   dofile
6       funcs   file:close
7       funcs   file:flush
8       funcs   file:lines
9       funcs   file:read
10      funcs   file:seek
11      funcs   file:setvbuf
12      funcs   file:write
13      funcs   loadfile

@usage Help("math","vars")
-- Regexp-match among topics - unsuccessful match returning suggestions instead
1       vars    math.huge
2       vars    math.maxinteger
3       vars    math.mininteger
4       vars    math.pi

@usage Help("ma.*","v..s")
-- Ditto above - explicit regexps

@usage Help("ma","va")
-- Ditto above - implicit regexps

@usage Help("math.pi","vars")
-- Explicit successful complete match - returning content
  math.pi

   The value of π.

--]]

-- If installed under xb-lua, lua-help is a built-in DSO. Version management
-- is required (append Lua version last in name)
local help = require("lua-help")

-- Declare a new empty table to become a class
Help = {};

-- Inherit from "help" and append CTOR
setmetatable(Help, {
    __call = function(_, ...)
            local sub, cat = ...

            if cat == nil and sub == nil then
                return Help:intro();
            else
                return Help:quasi( sub, cat );
            end
        end
    }
)
--[[--
Meta-table with constructor (class)

The class `Help` is used as disposable constructor with the purpose of
returning/output relevant text content. It's taking `0..2` arguments and
depending on which invokes `Help:quasi(sub, cat)` or `Help:intro ()`

@table Help

@field __call is used as syntactic sugar for constructor

@see intro
@see quasi

--]]

--[[--
High-level function

Try different methods until something is output. Most specific first.

@function quasi
@tparam string sub Subject literal or regexp [opt]
@tparam string cat Category literal or regexp [opt]
@treturn enum Fail or type of output ([0=fail], [1=doc], [2=topics])
--]]
function Help:quasi(sub, cat)
    rc = 0;
    if sub == nil and cat == nil then
        error("Both arguments can't be nil")
    end
    if self:info(sub, cat) == 0 then
        print("")
        print("Regexp-match among topics")
        if self:topics(sub, cat) == 0 then
            print("Nothing found neither among texts nor topics for", sub , cat);
            return 0;
        else
            print("");
            print("Close match topics found");
            print("");
            print("Select a subject from the list above and re-run the query" ..
                " to see its help-text.");
            print(" If subject alone prints content for another subject in" ..
                " another category,\n then the query is not specific enough." ..
                " Include also the category");
            return 2;
        end
    end
    return 1;
end

--[[--
Introductory help-text

Function takes no arguments just unconditionally prints it's text.

@function intro
@treturn string URL to the Reference manual on line
--]]
function Help:intro()
    local text = "    Welcome to Lua-help, a built-in reference or list of topics"
    local url = self:url()
    text = text .. "\n\n"
    text = text .. [=[
    You have entered Help() without arguments which is why you see this
    text. To use Help, call it with one or two arguments. Arguments can be
    either exact strings or regular expressions. ]=] ..

    "\n\n    Reference is for " .. _VERSION .. " (" .. url .. ")\n" .. [=[

    Depending on exact match for arguments interpreted as [subject] and [category],
    help text will be output. If not, arguments will be interpreted as
    regular expressions  and a list of topics topic  will be output.

    * Help:info( subject [, category] );
    * Help:topics( [resubject [, recategory]] );

    Most exact hit first and not stopping until either text or (list of) topics
    is found.

    Example:
    List all topics:
    > Help(".*");
    ]=]
    print(text);
    return url;
end

--[[--
Search for regular expression matches of subjects and/or categories.

Prints a list of none or topics depending of whats found

@function topics
@tparam string resub Subject regexp [opt]
@tparam string recat Category regexp [opt]
@treturn integer Number of topics printed
--]]
function Help:topics(resub, recat)
    local num=0

    if resub == nil then resub = ".*" end
    if recat == nil then recat = ".*" end

    for cat, sub in help.imatch( resub, recat ) do
        num = num +1;
        print(num, cat, sub);
    end
    return num
end

--[[--
Print help for subject

@function info
@tparam string sub Subject
@tparam string cat Category [opt]
@treturn integer number of characters output
--]]
function Help:info(sub, cat)
    if sub == nil then
        error("At least one argument is required")
    end
    if cat == nil then cat = "*" end
    text = help.help(sub, cat);

    if text == nil then
        return 0
    else
        print(text)
        return string.len(text)
    end
end

--[[--
Get URL for onlune Reference manual

The same source the module lua-help used when it composed the in-VM this
searchable/printable topics text i.e. Use when/if something is missing.

@function url
@treturn string URL to the relevant versions Reference manual on line
--]]
function Help:url()
    local V=string.gsub(_VERSION,"Lua ","");
    local url = "https://www.lua.org/manual/".. V .. "/manual.html"
    return url;
end
